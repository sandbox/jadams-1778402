<?php

// TODO: Look at class inheritance.

// Contest Controllers
class UserContest extends Entity{

  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'user-contest/' . $this->identifier());
  }
}

class UserContestController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'num_entries' => 1,
      'active' => 1,
			'entry_type' => '',
			'entry_data_type' => '',
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $contest = $entity;

    // Create author field
    $author = user_load($contest->uid);
		$name = theme('username', array('account' => $author));
		$created = format_date($contest->created);
		$content['author'] = array(
			'#type' => 'item',
			'#markup' => t('By !name on @created', array('!name' => $name, '@created' => $created))
		);

    // Create Entry Link Field
    if($contest->active){
      $path = $contest->uri();
      $contest_path = $path['path'];
      $content['enter_link'] = array(
        '#type' => 'item',
        '#markup' => l(t('Submit Entry'), $contest_path . '/submit/' . $contest->entry_type),
        '#weight' => 2000
      );
    }
    /*
		// Theme the contest
		$content += array(
			'#theme'     => 'contest',
			'#contest'   => $contest,
			'#view_mode' => $view_mode,
		);
		*/
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}


// Contest Entry Controllers.
class UserContestEntry extends UserContest {

  protected function defaultLabel() {
    $contest = user_contest_load($this->cid);
    return $contest->title . ' Entry';
  }

  protected function defaultUri() {
    return array('path' => 'user-contest-entry/' . $this->identifier());
  }
}

// This function really isn't needed since we will never view the entry itself
// from the front end but its here just in case.
class UserContestEntryController extends EntityAPIController {
	public function create(array $values = array()) {
    global $user;
    $values += array(
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }
}

// Contest Entry Data Controllers.
class UserContestEntryData extends UserContest {

  protected function defaultUri() {
    return array('path' => 'user-contest-entry-data/' . $this->identifier());
  }
}

class UserContestEntryDataController extends EntityAPIController {
	public function create(array $values = array()) {
    global $user;
    $values += array(
      'title' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $contest_entry_data = $entity;

    // Create author field
    $author = user_load($contest_entry_data->uid);
		$name = theme('username', array('account' => $author));
		$created = format_date($contest_entry_data->created);
		$content['author'] = array(
			'#type' => 'item',
			'#markup' => t('By !name on @created', array('!name' => $name, '@created' => $created))
		);

    $vote_options = array(
      'entity_id' => $contest_entry_data->did,
      'type' => 'user_contest_entry_data',
      'tag' => 'user_contest_entry_data',
      'widget_theme' => 'updown',
    );
    $content['voting_widget'] = array(
      '#type' => 'item',
      '#markup' => theme('vud_widget', $vote_options)
    );

		return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

// User Contest Types Controllers
class DefaultUserContestType extends Entity{
  public $type;
  public $label;
  public $weight = 0;

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}

class UserContestType extends DefaultUserContestType{
  public function __construct($values = array()) {
    parent::__construct($values, 'user_contest_type');
  }
}

class UserContestEntryType extends DefaultUserContestType{
  public function __construct($values = array()) {
    parent::__construct($values, 'user_contest_entry_type');
  }
}

class UserContestEntryDataType extends DefaultUserContestType{
  public function __construct($values = array()) {
    parent::__construct($values, 'user_contest_entry_data_type');
  }
}

class DefaultUserContestTypeController extends EntityAPIControllerExportable{
   public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
    );
    return parent::create($values);
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    variable_set('menu_rebuild_needed', TRUE);
  }
}
