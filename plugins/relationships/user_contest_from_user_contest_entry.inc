<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('User Contest from User Contest Entry'),
  'keyword' => 'user_contest',
  'description' => t('Adds a User Contest from User Contest Entry.'),
  'required context' => new ctools_context_required(t('User Contest Entry'), 'user_contest_entry'),
  'context' => 'user_contest_entry_from_user_contest_entry',
  'settings form' => 'user_contest_entry_from_user_contest_entry_settings_form',
);

/**
 * Return a new context based on an existing context.
 */
function user_contest_entry_from_user_contest_entry($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('entity:user_contest', NULL);
  }

  $user_contest_entry = user_contest_load($context->data->cid);
  return ctools_context_create('entity:user_contest', $user_contest_entry);
}

/**
 * Settings form for the relationship.
 */
function user_contest_entry_from_user_contest_entry_settings_form($conf) {
  // We won't configure it in this case.
  return array();
}



