<?php

/**
 * @file
 *
 * Sample relationship plugin.
 *
 * We take a simplecontext, look in it for what we need to make a relcontext, and make it.
 * In the real world, this might be getting a taxonomy id from a node, for example.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('User Contest Entry from User Contest Entry Data'),
  'keyword' => 'user_contest_entry',
  'description' => t('Adds a User Contest Entry from User Contest Entry Data.'),
  'required context' => new ctools_context_required(t('User Contest Entry Data'), 'user_contest_entry_data'),
  'context' => 'user_contest_entry_from_user_contest_entry_data',
  'settings form' => 'user_contest_entry_from_user_contest_entry_data_settings_form',
);

/**
 * Return a new context based on an existing context.
 */
function user_contest_entry_from_user_contest_entry_data($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('entity:user_contest_entry', NULL);
  }

  $user_contest_entry = user_contest_entry_load($context->data->eid);
  return ctools_context_create('entity:user_contest_entry', $user_contest_entry);
}

/**
 * Settings form for the relationship.
 */
function user_contest_entry_from_user_contest_entry_data_settings_form($conf) {
  // We won't configure it in this case.
  return array();
}


