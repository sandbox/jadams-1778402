<?php

$plugin = array(
  'title' => t('User Contest Entry Edit Form'),
  'description' => t('Displays an entry form for the given contest'),
  'single' => TRUE,
  'content_types' => array('user_contest_entry_edit_form_content_type'),
  'render callback' => 'user_contest_entry_edit_form_content_type_render',
  'defaults' => array(),
  'required context' => new ctools_context_required(t('User Contest Entry'), 'user_contest_entry'),
  'category' => array(t('User Contest Entry')),
);

function user_contest_entry_edit_form_content_type_render($subtype, $conf, $args, $context) {
  
  ctools_include('user_contest.admin', 'user_contest', '');
  $user_contest_entry = $context->data;
  
  $block = new stdClass();
  $block->title = '';
  $block->content = drupal_get_form('user_contest_entry_form',$user_contest_entry);
  
  return $block;
}
