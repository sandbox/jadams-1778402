<?php

// user_contest_entry_data_pager
$plugin = array(
  'title' => t('User Contest Entry Data Pager'),
  'description' => t('Display Article Pager'),
  'single' => TRUE,
  'content_types' => array('user_contest_entry_data_pager_content_type'),
  'render callback' => 'user_contest_entry_data_pager_content_type_render',
  'defaults' => array(),
  'required context' => new ctools_context_required(t('User Contest Entry Data'), 'user_contest_entry_data'),
  'category' => array(t('User Contest Entry Data')),
);

function user_contest_entry_data_pager_content_type_render($subtype, $conf, $args, $context) {

  $user_contest_entry_data = $context->data;
  
  $prev_link = '';
  $next_link = '';
  
  $pager = user_contest_entry_data_pager_query($user_contest_entry_data);
  if (!empty($pager['prev'])){
    $prev_link = l(t('Prev'), 'user-contest-entry-data/' . $pager['prev']->did, array('attributes' => array('class' => array('prev-link'))));
  }
  if (!empty($pager['next'])){
    $next_link = l(t('Next'), 'user-contest-entry-data/' . $pager['next']->did, array('attributes' => array('class' => array('next-link'))));
  }
  
  $options = array(
    '!prev' => $prev_link,
    '!next' => $next_link,
    '@index' => $pager['index'],
    '@total' => $pager['total']
  );
  
  $block = new stdClass();
  $block->title = '';
  $block->content = t('!prev @index of @total !next', $options);
  return $block;
}

function user_contest_entry_data_pager_query($user_contest_entry_data){
  
  $user_contest_entry = user_contest_entry_load($user_contest_entry_data->eid);
  
  $query = db_select('user_contest_entry_data', 'd');
  $query->join('user_contest_entry', 'e', 'd.eid = e.eid');
  $query->join('user_contest', 'c', 'e.cid = c.cid');
  $query->fields('d',array('did','title'))
    ->condition('c.cid', $user_contest_entry->cid)
    ->orderBy('d.created', 'DESC');
    
  $results = $query->execute()->fetchAll();
  foreach ($results as $delta => $result){
    if ($result->did == $user_contest_entry_data->did){
      $current_delta = $delta;
    }
  }
  
  $return = array(
    'prev' => isset($results[$current_delta - 1]) ? $results[$current_delta - 1] : NULL,
    'next' => isset($results[$current_delta + 1]) ? $results[$current_delta + 1] : NULL,
    'index' => $current_delta + 1,
    'total' => count($results),
  );

  return $return;
}