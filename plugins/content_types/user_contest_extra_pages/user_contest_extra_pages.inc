<?php

$plugin = array(
  'title' => t('User Contest Extra Pages'),
  'description' => t('Displays extra pages for the given contest'),
  'single' => TRUE,
  'content_types' => array('user_contest_extra_pages_content_type'),
  'render callback' => 'user_contest_extra_pages_content_type_render',
  'defaults' => array(),
  'required context' => new ctools_context_required(t('User Contest'), 'user_contest'),
  'category' => array(t('User Contest')),
);

function user_contest_extra_pages_content_type_render($subtype, $conf, $args, $context) {
    
  $user_contest = $context->data;

  $block = new stdClass();
  $block->title = '';
  $block->content = user_contest_extra_page($user_contest, $args[1]);
  
  return $block;
}
