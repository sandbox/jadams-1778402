<?php

$plugin = array(
  'title' => t('User Contest Entry Form'),
  'description' => t('Displays an entry form for the given contest'),
  'single' => TRUE,
  'content_types' => array('user_contest_entry_form_content_type'),
  'render callback' => 'user_contest_entry_form_content_type_render',
  'defaults' => array(),
  'required context' => new ctools_context_required(t('User Contest'), 'user_contest'),
  'category' => array(t('User Contest')),
);

function user_contest_entry_form_content_type_render($subtype, $conf, $args, $context) {
  
  ctools_include('user_contest.admin', 'user_contest', '');
  $user_contest = $context->data;
  
  $block = new stdClass();
  $block->title = '';
  $block->content = user_contest_add($user_contest->entry_type, $user_contest, $entity = 'user_contest_entry');
  
  return $block;
}
