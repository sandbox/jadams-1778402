<?php

function user_contest_admin(){
  $output = '<dl class="node-type-list">';
  $output .= '<dt>' . l('User Contest', 'admin/content/multi-blog/blog-channel') . '</dt>';
  $output .= '<dd> The wrapper for Multi Blogs.</dd>';
  $output .= '<dt>' . l('User Contest Entry', 'admin/content/multi-blog/blog') . '</dt>';
  $output .= '<dd> The wrapper for Multi Blog Posts.</dd>';
  $output .= '<dt>' . l('User Contest Entry Data', 'admin/content/multi-blog/blog-posts') . '</dt>';
  $output .= '<dd> The default node for Multi Blog Posts.</dd>';
  $output .= '</dl>';

  return $output;
}

/**
 * User Contest Type Edit Form
 */
function user_contest_type_form($form, &$form_state, $user_contest_type, $op = 'edit') {

  if ($op == 'clone') {
    $user_contest_type->label .= ' (cloned)';
    $user_contest_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $user_contest_type->label,
    '#description' => t('The human-readable name of this contest type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($user_contest_type->type) ? $user_contest_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $user_contest_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'user_contest_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this contest type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($user_contest_type->description) ? $user_contest_type->description : '',
    '#description' => t('Description about the contest type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Contest Type'),
    '#weight' => 40,
  );

  if (!$user_contest_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete contest type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('user_contest_type_delete_redirect')
    );
  }
  return $form;
}

/**
 * Submit Handler for Contest Type Edit Form
 */
function user_contest_type_form_submit($form, &$form_state){
	$user_contest_type = entity_ui_form_submit_build_entity($form, $form_state);
	$user_contest_type->save();
	$form_state['redirect'] = 'admin/structure/user-contest/contest-types';
}

/**
 * Redirect for Contest Type Edit Delete.
 */
function user_contest_type_delete_redirect($form, &$form_state){
	$form_state['redirect'] = 'admin/structure/user-contest/contest-types/' . $form_state['user_contest_type']->type . '/delete';
}

/**
 * User Contest Entry Type Edit Form
 */
function user_contest_entry_type_form($form, &$form_state, $user_contest_type, $op = 'edit') {

  if ($op == 'clone') {
    $user_contest_type->label .= ' (cloned)';
    $user_contest_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $user_contest_type->label,
    '#description' => t('The human-readable name of this contest type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($user_contest_type->type) ? $user_contest_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $user_contest_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'user_contest_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this contest type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($user_contest_type->description) ? $user_contest_type->description : '',
    '#description' => t('Description about the contest type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Contest Entry Type'),
    '#weight' => 40,
  );

  if (!$user_contest_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Contest Entry Type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('user_contest_entry_type_delete_redirect')
    );
  }
  return $form;
}

/**
 * Submit Handler for Entry Type Edit Form
 */
function user_contest_entry_type_form_submit($form, &$form_state){
	$user_contest_type = entity_ui_form_submit_build_entity($form, $form_state);
	$user_contest_type->save();
	$form_state['redirect'] = 'admin/structure/user-contest/contest-entry-types';
}

/**
 * Redirect for Entry Type Edit Delete
 */
function user_contest_entry_type_delete_redirect($form, &$form_state){
	$form_state['redirect'] = 'admin/structure/user-contest/contest-entry-types/' . $form_state['user_contest_type']->type . '/delete';
}

/**
 * User Contest Entry Data Type Edit Form
 */
function user_contest_entry_data_type_form($form, &$form_state, $user_contest_entry_data_type, $op = 'edit') {

  if ($op == 'clone') {
    $user_contest_entry_data_type->label .= ' (cloned)';
    $user_contest_entry_data_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $user_contest_entry_data_type->label,
    '#description' => t('The human-readable name of this contest type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($user_contest_entry_data_type->type) ? $user_contest_entry_data_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $user_contest_entry_data_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'user_contest_entry_data_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this contest type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($user_contest_entry_data_type->description) ? $user_contest_entry_data_type->description : '',
    '#description' => t('Description about the contest type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Contest Entry Data Type'),
    '#weight' => 40,
  );

  if (!$user_contest_entry_data_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Contest Entry Data Type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('user_contest_entry_data_type_delete_redirect')
    );
  }
  return $form;
}

/**
 * Submit Handler for Entry Type Data Edit Form
 */
function user_contest_entry_data_type_form_submit($form, &$form_state){
	$user_contest_entry_data_type = entity_ui_form_submit_build_entity($form, $form_state);
	$user_contest_entry_data_type->save();
	$form_state['redirect'] = 'admin/structure/user-contest/contest-entry-data-types';
}

/**
 * Redirect for Entry Type Edit Form Delete
 */
function user_contest_entry_data_type_delete_redirect($form, &$form_state){
	$form_state['redirect'] = 'admin/structure/user-contest/contest-entry-data-types/' . $form_state['user_contest_type']->type . '/delete';
}

// TODO: Take another look at this function.
/**
 * Helper function to determine what form needs to be loaded based
 * on contest/contest entry type.
 */
function user_contest_add($type, $parent = NULL, $entity = 'user_contest'){
	// Create a switch case in here so this can be used for all types.
	switch($entity){
		case 'user_contest':
			$user_contest_type = user_contest_types($type);
			$user_contest = entity_create('user_contest', array('type' => $type));
			drupal_set_title(t('Create @name', array('@name' => entity_label('user_contest_type', $user_contest_type))));
			$form =  drupal_get_form('user_contest_form', $user_contest);
			break;

	 case 'user_contest_entry':
			$user_contest = $parent;
			$values = array(
			  'type' => $type,
				'cid' => $user_contest->cid,
			);
			$user_contest_entry = entity_create('user_contest_entry', $values);
			drupal_set_title(t('Enter @title Contest', array('@title' => $user_contest->title)));
			$form =  drupal_get_form('user_contest_entry_form', $user_contest_entry, 'add');
			break;
	 case 'user_contest_entry_data':
			$user_contest_entry = $parent;
			$values = array(
			  'type' => $type,
				'eid' => $user_contest_entry->eid,
			);
			$user_contest_entry_data = entity_create('user_contest_entry_data', $values);
			drupal_set_title(t('Contest Entry'));
			$form =  drupal_get_form('user_contest_entry_data_form', $user_contest_entry_data, 'add');
			break;
	  default:
			$form = drupal_not_found();
			break;
	}
  return $form;

}

/**
 * User Contest Edit Form.
 */
function user_contest_form($form, &$form_state, $user_contest) {
  $form_state['user_contest'] = $user_contest;

	if (!empty($user_contest->title)){
		drupal_set_title($user_contest->title);
	} else {
		drupal_set_title(t('Add Contest'));
	}

  !empty($user_contest->cid) ? $op = 'edit' : $op = 'add';
  $form_state['op'] = $op;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
    '#default_value' => $user_contest->title,
  );

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 999
  );

    $form['types'] = array(
      '#type' => 'fieldset',
      '#title' => t('Entry Types'),
      '#group' => 'additional_settings'
    );

      $entry_types = array();
      foreach (user_contest_entry_types() as $type){
        $entry_types[$type->type] = $type->label;
      }

      if (empty($entry_types)){
        drupal_set_message(t('Please create a !link before adding a contest', array('!link' => l(t('Contest Entry Type'),'admin/structure/user-contest/contest-entry-types'))),'error');
      }

      $form['types']['entry_type'] = array(
        '#type' => 'select',
        '#title' => t('Entry Types'),
        '#options' => $entry_types,
        '#default_value' => $user_contest->entry_type,
        '#required' => TRUE
      );

      $entry_data_types = array();
      foreach (user_contest_entry_data_types() as $type){
        $entry_data_types[$type->type] = $type->label;
      }

      if (empty($entry_data_types)){
        drupal_set_message(t('Please create a !link before adding a contest', array('!link' => l(t('Contest Data Entry Type'),'admin/structure/user-contest/contest-entry-data-types'))),'error');
      }

      $form['types']['entry_data_type'] = array(
        '#type' => 'select',
        '#title' => t('Entry Data Types'),
        '#options' => $entry_data_types,
        '#default_value' => $user_contest->entry_data_type,
        '#required' => TRUE
      );

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Contest Settings'),
      '#group' => 'additional_settings'
    );

      $form['settings']['active'] = array(
        '#type' => 'radios',
        '#title' => t('Contest Status'),
        '#options' => array(1 => 'Open', 0 => 'Closed'),
        '#default_value' => $user_contest->active
      );

      $options = drupal_map_assoc(array(1,2,3,4,5,6,7,8,9,10));
      $form['settings']['num_entries'] = array(
        '#type' => 'select',
        '#title' => t('Number of Entries per Submission'),
        '#options' => $options,
        '#default_value' => $user_contest->num_entries
      );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $user_contest->uid,
  );

	$form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional Pages'),
    '#group' => 'additional_settings'
  );

  field_attach_form('user_contest', $user_contest, $form, $form_state);

    // If path is enabled show the path settings.
  if (module_exists('path')) {
    user_contest_path_settings('user_contest', $user_contest, $form);
  }

  if (module_exists('pathauto')){
    pathauto_field_attach_form('user_contest', $user_contest, $form, $form_state, LANGUAGE_NONE);
  }

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 1000,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Contest'),
  );

  // Show Delete button if we edit contest.
  $user_contest_id = entity_id('user_contest' ,$user_contest);
  if (!empty($user_contest_id) && user_contest_access('edit', $user_contest)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('subform_submit_all','user_contest_delete_form_submit'),
    );
  }

  $form['#validate'][] = 'user_contest_form_validate';

  return $form;
}

/**
 * Validate function for Contest Edit Form.
 */
function user_contest_form_validate($form, &$form_state){

}

/**
 * Submit Handler for Contest Edit Form.
 */
function user_contest_form_submit($form, &$form_state){
	$user_contest = $form_state['user_contest'];
  $op = $form_state['op'];
  entity_form_submit_build_entity('user_contest', $user_contest, $form, $form_state);

	// I need to save the first time to get an alias and cid.
	$user_contest->save();

  if (module_exists('path')) {
    user_contest_path_save('user_contest', $user_contest);
  }

  // If pathauto is enabled we create an alias automatically.
  if (module_exists('pathauto') && !empty($form_state['values']['path']['pathauto'])) {
    if ($op == 'add'){
      user_contest_create_alias($user_contest, 'insert');
    } else if ($op == 'edit'){
      user_contest_create_alias($user_contest, 'update');
    }
  }

  $user_contest_uri = $user_contest->uri();

  // TODO: Save Pages Field to links
  /*
  if ($user_contest->user_contest_pages && $user_contest->user_contest_links){
    $user_contest_wrapper = entity_metadata_wrapper('user_contest', $user_contest);

    $user_contest_pages = $user_contest_wrapper->user_contest_pages->value();
    foreach ($user_contest_pages as $page){

      $link = array(
        'title' => $page['title'],
        'url' => 'user-contest/' . $user_contest->cid . '/' . $page['uri'],
      );
      $user_contest_wrapper->user_contest_links[] = $link;
    }

    $user_contest_wrapper->save();
  }
  */



  // Redirect to Contest Page
  $form_state['redirect'] = $user_contest_uri['path'];
  drupal_set_message(t('Contest %title saved.', array('%title' => entity_label('user_contest', $user_contest))));
}

/**
 * User Contest Delete Form.
 *
 * @param type $form
 * @param array $form_state
 * @param type $user_contest
 * @return type
 */
function user_contest_delete_form($form, &$form_state, $user_contest) {
  $form_state['user_contest'] = $user_contest;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['user_contest_type_id'] = array('#type' => 'value', '#value' => entity_id('user_contest', $user_contest));
  $user_contest_uri = entity_uri('user_contest', $user_contest);
  return confirm_form($form,
    t('Are you sure you want to delete contest %title?', array('%title' => entity_label('user_contest', $user_contest))),
    $user_contest_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete handler for Contest Edit Form.
 *
 * @param type $form
 * @param array $form_state
 */
function user_contest_delete_form_submit($form, &$form_state){
	$user_contest = $form_state['user_contest'];

  // Delete Contest Entity and redirect
  // Grab all associated entries and entry data and delete as well.
  contest_delete($user_contest);
  drupal_set_message(t('Contest %title deleted.', array('%title' => entity_label('user_contest', $user_contest))));
  $form_state['redirect'] = '<front>';
}

/**
 * User Contest Entry Edit Form.
 *
 * @param type $form
 * @param type $form_state
 * @param type $user_contest_entry
 * @param type $op
 * @return string
 */
function user_contest_entry_form($form, &$form_state, $user_contest_entry, $op = 'edit') {

	drupal_set_title(t('Contest Entry Form'));

	$user_contest = user_contest_load($user_contest_entry->cid);
  $form_state['user_contest'] = $user_contest;

	if (!$user_contest->active){
		$form['info'] = array(
      '#type' => 'item',
      '#markup' => t('This contest is closed and not accepting entries at this time.'),
    );
		return $form;
	}

  $form_state['user_contest_entry'] = $user_contest_entry;

	$form['uid'] = array(
    '#type' => 'value',
    '#value' => $user_contest_entry->uid,
  );

  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Personal Info'),
  );

  // Generates Fields from Field Api
  field_attach_form('user_contest_entry', $user_contest_entry, $form['info'], $form_state);

  $form['data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entries'),
    '#tree' => TRUE,
    '#prefix' => '<div id="entry-data-form">',
    '#suffix' => '</div>',
  );

  // Create fields for contest entry.
  // On edit load all entry data associated
	if ($op == 'edit'){
		if (empty($form_state['entry_data'])){
      $form_state['entry_data'] = user_contest_get_entry_data($user_contest_entry);
		}
  } else if(empty($form_state['entry_data'])){
		$values = array(
      'type' => $user_contest->entry_data_type,
      'cid' => $user_contest->cid,
    );
    $form_state['entry_data'][] = entity_create('user_contest_entry_data', $values);
	}

  foreach($form_state['entry_data'] as $key => $entry_data){

		$form['data'][$key] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="entry-data-form-' . $key . '">',
      '#suffix' => '</div>',
    );

    $form['data'][$key]['entry_data'] = array(
      '#type' => 'subform',
      '#subform_id' => 'user_contest_entry_data_form',
      '#subform_arguments' => array($entry_data,'add'),
			'#subform_file' => array('module' => 'user_contest', 'name' => 'user_contest.admin'),
			'#data_separation' => FALSE,
			'#required' => TRUE
    );

		if (count($form_state['entry_data']) > 1){
			$form['data'][$key]['remove_entry'] = array(
				'#type' => 'submit',
				'#name' => 'remove-' .  $key,
				'#value' => t('Remove'),
				'#submit' => array('user_contest_entry_remove_handler'),
				//'#limit_validation_errors' => TRUE,
				'#ajax' => array(
					'callback' => 'user_contest_entry_form_ajax_rebuild',
					'wrapper' => 'entry-data-form',
				),
			);
		}

  }

	if (count($form_state['entry_data']) < $user_contest->num_entries){
		$form['data']['add_entry'] = array(
			'#type' => 'submit',
			'#value' => t('Add another entry'),
			'#submit' => array('user_contest_entry_add_another_handler'),
			'#subform_triggering_element' => array(
        'user_contest_entry_data_form' => array('actions', 'submit'),
      ),
			'#ajax' => array(
				'callback' => 'user_contest_entry_form_ajax_rebuild',
				'wrapper' => 'entry-data-form',
			),
		);
	}

  $form['actions'] = array(
    '#weight' => 100,
  );

	if (isset($user_contest_entry->eid)){
		$form['actions']['delete'] = array(
			'#type' => 'submit',
			'#value' => t('Delete'),
			'#submit' => array('user_contest_entry_delete_form_submit'),
		);
	}

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
		'#submit' => array('user_contest_entry_form_submit'),
		// This is needed to trigger validation for the subform elements.
		'#subform_triggering_element' => array(
      'user_contest_entry_data_form' => array('actions', 'submit'),
    ),

  );

	// See http://drupal.org/node/133725
	$form['#attributes'] = array('enctype' => 'multipart/form-data');
	return $form;
}

/**
 * Ajax callback for Contest Entry Edit Form. Rebuild the array
 * that holds the entry data.
 */
function user_contest_entry_form_ajax_rebuild($form, $form_state){
	return $form['data'];
}

/**
 * Callback for "Add Another Entry", Creates a new blank entry and adds
 * it to the form.
 */
function user_contest_entry_add_another_handler($form, &$form_state){
  $user_contest = $form_state['user_contest'];
  $values = array(
    'type' => $user_contest->entry_data_type,
    'cid' => $user_contest->cid,
  );

  $form_state['entry_data'][] = entity_create('user_contest_entry_data', $values);
	$form_state['rebuild'] = TRUE;
}

/**
 * Callback for "Remove", removes entry data from the array and adds it to
 * another so that it can be deleted at submit.
 */
function user_contest_entry_remove_handler($form, &$form_state){

  $key = $form_state['triggering_element']['#parents'][1];

  // Remove entry_data data and add it to the deleted_entry_data
	// and rebuild the form.
	$form_state['deleted_entry_data'][] = $form_state['entry_data'][$key];
	unset($form_state['entry_data'][$key]);
	$form_state['rebuild'] = TRUE;

}

/**
 * Submit handler for Contest Entry Edit Form
 */
function user_contest_entry_form_submit($form, &$form_state){

  // Save the contest entry
  $user_contest_entry = $form_state['user_contest_entry'];

	$user_contest = user_contest_load($user_contest_entry->cid);
	$user_contest_path = $user_contest->uri();
	//$user_contest_alias = drupal_lookup_path('alias', $user_contest_path['path']);

  entity_form_submit_build_entity('user_contest_entry', $user_contest_entry, $form, $form_state);
  $user_contest_entry->save();

  foreach ($form_state['entry_data'] as $key => $user_contest_entry_data){

		$subform = $form['data'][$key]['entry_data']['#subform'];
		$subform_name = $form['data'][$key]['entry_data']['#name'];
		$subform_state = &subform_get_state($subform_name, $form_state);

		// Create Contest Entry Data Object
		entity_form_submit_build_entity('user_contest_entry_data', $user_contest_entry_data, $subform, $subform_state);
    $user_contest_entry_data->eid = $user_contest_entry->eid;
    $user_contest_entry_data->save();

		// Set alias for contest entry data if one does not exist already.
		/*
    $data_path = 'user_contest/'.$user_contest->cid.'/entry/'.$user_contest_entry->eid.'/data/'.$user_contest_entry_data->did;
		if (!drupal_lookup_path('alias', $data_path)){
			$path = array(
        'source' => $data_path,
				'alias' => $user_contest_alias . '/data/' .  $user_contest_entry_data->did
			);
			path_save($path);
		}
    */

  }

	// Delete all contest entry data that wasn't saved.
  if (isset($form_state['deleted_entry_data'])){
    user_contest_entry_data_delete_multiple($form_state['deleted_entry_data']);
  }
  drupal_set_message('Your entries have been submitted');
	$form_state['redirect'] = $user_contest_path['path'];
}

/**
 * User Contest Entry Delete Form
 */
function user_contest_entry_delete_form($form, &$form_state, $user_contest_entry) {
  $form_state['user_contest_entry'] = $user_contest_entry;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['user_contest_entry_type_id'] = array('#type' => 'value', '#value' => entity_id('user_contest_entry', $user_contest_entry));
  $user_contest_uri = entity_uri('user_contest_entry', $user_contest_entry);
  return confirm_form($form,
    t('Are you sure you want to delete this contest entry. All data associated with this entry will be deleted'),
    $user_contest_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for Contest Entry Delete Form. This function also deletes all
 * User Contest Entry Data associated with this contest.
 */
function user_contest_entry_delete_form_submit($form, &$form_state){
	$user_contest_entry = $form_state['user_contest_entry'];
  contest_entry_delete($user_contest_entry);
  drupal_set_message(t('Contest Entry Deleted'));

  $query = db_select('user_contest_entry_data', 'd')
    ->condition('d.eid', $user_contest_entry->eid, '=')
    ->fields('d', array('did'));

  $result = $query->execute()->fetchAll();

	// Delete all entry data associated with this entry.
  foreach ($result as $obj){
		contest_entry_data_delete($obj->did);
  }

  $form_state['redirect'] = '<front>';
}

/**
 * User Contest Entry Data Edit Form. This form is pulled into the Contest Entry Edit Form,
 * by means of the subform module.
 */
function user_contest_entry_data_form($form, &$form_state, $user_contest_entry_data, $op = 'edit') {

  $form_state['user_contest_entry_data'] = $user_contest_entry_data;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
    '#default_value' => $user_contest_entry_data->title
  );

  // Create new contest entry data and attach fields from field api.
  $form_state['user_contest_entry_data'] = $user_contest_entry_data;
  field_attach_form('user_contest_entry_data', $user_contest_entry_data, $form, $form_state);

	$form['actions'] = array(
    '#weight' => 100,
  );

	// Subform requires a default triggering element. So this submit is here but
	// hidden from view.
	$form['actions']['submit'] = array(
    '#type' => 'submit',
		'#value' => t('Submit'),
		'#prefix' => '<div style="display:none">',
		'#suffix' => '</div>',
	);

	return $form;
}

function user_contest_entry_data_form_submit($form, &$form_state){

}

// Utility Functions.
function user_contest_path_settings($entity_type, $entity, &$form){

  $url_prefix = str_replace('_', '-', $entity_type);
  $id = entity_id($entity_type ,$entity);

  $path = array();
  if (!empty($id)) {

    $conditions = array('source' => $url_prefix . '/' . $id);
    $path = path_load($conditions);

    if ($path === FALSE) {
      $path = array();
    }
  }
  $path += array(
    'pid' => NULL,
    'source' => isset($id) ? $url_prefix . '/' . $id : NULL,
    'alias' => '',
    'language' => LANGUAGE_NONE,
  );

  $form['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('URL path settings'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($path['alias']),
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('path-form'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'path') . '/path.js'),
    ),
    '#access' => user_access('create url aliases') || user_access('administer url aliases'),
    '#weight' => 30,
    '#tree' => TRUE,
    '#element_validate' => array('path_form_element_validate'),
  );
  $form['path']['alias'] = array(
    '#type' => 'textfield',
    '#title' => t('URL alias'),
    '#default_value' => $path['alias'],
    '#maxlength' => 255,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Optionally specify an alternative URL by which this content can be accessed. For example, type "about" when writing an about page. Use a relative path and don\'t add a trailing slash or the URL alias won\'t work.'),
  );
  $form['path']['pid'] = array('#type' => 'value', '#value' => $path['pid']);
  $form['path']['source'] = array('#type' => 'value', '#value' => $path['source']);
  $form['path']['language'] = array('#type' => 'value', '#value' => $path['language']);

}

function user_contest_path_save($entity_type, $entity){

  $url_prefix = str_replace('_', '-', $entity_type);
  $id = entity_id($entity_type ,$entity);

  if (isset($entity->path)) {
    $path = $entity->path;
    $path['alias'] = trim($path['alias']);
    // Only save a non-empty alias.
    if (!empty($path['alias'])) {
      // Ensure fields for programmatic executions.
      $path['source'] = $url_prefix . '/' . $id;
      $path['language'] = LANGUAGE_NONE;
      path_save($path);
    }
  }
}
