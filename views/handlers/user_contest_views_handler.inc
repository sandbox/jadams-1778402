<?php

class ViewsHandlerFieldUserContestLink extends views_handler_field_entity {
  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::options_form($form, $form_state);

    // The path is set by render_link function so don't allow to set it.
    $form['alter']['path'] = array('#access' => FALSE);
    $form['alter']['external'] = array('#access' => FALSE);
  }

  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_link($entity, $values);
    }
  }

  function render_link($user_contest, $values) {
    if (user_contest_access('view', $user_contest)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "user-contest/$user_contest->cid";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('View');
      return $text;
    }
  }
}

class ViewsHandlerFieldUserContestLinkEdit extends ViewsHandlerFieldUserContestLink {

  function render_link($user_contest, $values) {
    if (user_contest_access('edit', $user_contest)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "user-contest/$user_contest->bid/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');
      return $text;
    }
  }
}

class ViewsHandlerFieldUserContestLinkDelete extends ViewsHandlerFieldUserContestLink {

  function render_link($user_contest, $values) {
    if (user_contest_access('edit', $user_contest)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "user-contest/$user_contest->bid/delete";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Delete');
      return $text;
    }
  }
}

class ViewsHandlerFieldUserContestEntryLink extends ViewsHandlerFieldUserContestLink {

  function render_link($user_contest_entry, $values) {
    if (user_contest_entry_access('view', $user_contest_entry)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "user-contest-entru/$user_contest_entry->eid";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('View');
      return $text;
    }
  }
}

class ViewsHandlerFieldUserContestEntryLinkEdit extends ViewsHandlerFieldUserContestLink {

  function render_link($user_contest_entry, $values) {
    if (user_contest_entry_access('edit', $user_contest_entry)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "user-contest-entry/$user_contest_entry->eid/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Edit');
      return $text;
    }
  }
}

class ViewsHandlerFieldUserContestEntryLinkDelete extends ViewsHandlerFieldUserContestLink {

  function render_link($user_contest_entry, $values) {
    if (user_contest_entry_access('delete', $user_contest_entry)) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = "user-contest-entry/$user_contest_entry->eid/edit";
      $text = !empty($this->options['text']) ? $this->options['text'] : t('Delete');
      return $text;
    }
  }
}
